﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Design;
using System.Linq;
using System.Web;
using DAL;
using DAL.Entities;
using Microsoft.AspNet.Identity;
using AuthorizeAttribute = System.Web.Mvc.AuthorizeAttribute;

namespace REST.Authorization
{
    public class RequireAdmin : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
         {
            var userId = httpContext.User.Identity.GetUserId();
            var user = new Facade().GetUserRepository().Read(userId);
            return user.IsAdmin;
        }
    }
}