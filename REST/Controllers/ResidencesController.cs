﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using DAL;
using DAL.Entities;
using DAL.Repository;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using REST.Models;
using DbContext = DAL.Context.DbContext;

namespace REST.Controllers {
    [RoutePrefix("api/Residences")]
    public class ResidencesController : ApiController {
        private readonly IRepository<Residence, int> _residenceRepository = new Facade().GetResidenceRepository();

        // GET: api/Residences
        public List<ResidenceGetBindingModel> GetResidences() {
            var residences = _residenceRepository.Read();
            residences.Reverse();

            var models = new List<ResidenceGetBindingModel>();
            foreach (var residence in residences) {
                models.Add(GetModelFromResidence(residence));
            }

            return models;
        }

        // GET: api/Residences
        public List<ResidenceGetBindingModel> GetResidences(int index, int amount)
        {
            var residences = _residenceRepository.Read();
            residences.Reverse();

//            if (residences.Count < index + amount) {
//                return new List<ResidenceGetBindingModel>();
//            }

            //residences = residences.GetRange(index, amount);
            residences = residences.Skip(index).Take(amount).ToList();

            var models = new List<ResidenceGetBindingModel>();
            foreach (var residence in residences)
            {
                models.Add(GetModelFromResidence(residence));
            }

            return models;
        }

        [Route("Search")]
        public List<ResidenceGetBindingModel> GetResidences(string input) {
            var residences = _residenceRepository.Read();
            residences.Reverse();

            var returnList = new List<ResidenceGetBindingModel>();

            if (input.IsNullOrWhiteSpace()) {
                foreach (var residence in residences) {
                    returnList.Add(GetModelFromResidence(residence));
                }
            }
            else {
                foreach (var residence in residences) {
                    if (residence.Address.Street.ToLower().Contains(input.ToLower())) {
                        returnList.Add(GetModelFromResidence(residence));
                        continue;
                    }

                    if (residence.Address.City.ToLower().Contains(input.ToLower()))
                    {
                        returnList.Add(GetModelFromResidence(residence));
                        continue;
                    }
                    if (residence.Address.Zip.ToString().Equals(input))
                    {
                        returnList.Add(GetModelFromResidence(residence));
                        continue;
                    }
                    if (residence.Type.ToLower().Contains(input.ToLower()))
                    {
                        returnList.Add(GetModelFromResidence(residence));
                        continue;
                    }
                }
            }

            return returnList;
        }

        // GET: api/Residences/5
        [ResponseType(typeof(ResidenceGetBindingModel))]
        public IHttpActionResult GetResidence(int id) {
            Residence residence = _residenceRepository.Read(id);
            if (residence == null) {
                return NotFound();
            }

            return Ok(GetModelFromResidence(residence));
        }

        // PUT: api/Residences/5
        [Authorize]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutResidence(int id, Residence residence) {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            if (id != residence.Id) {
                return BadRequest();
            }

            _residenceRepository.Update(residence);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Residences
        [Authorize]
        [ResponseType(typeof(Residence))]
        public IHttpActionResult PostResidence(ResidencePostBindingModel model) {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            var residence = new Residence
            {
                Aconto = model.Aconto.Value,
                Address = model.Address,
                Animal = model.Animal.Value,
                Deposit = model.Deposit.Value,
                Description = model.Description,
                Energy = model.Energy,
                Furnished = model.Furnished.Value,
                Images = model.Images,
                PrepaidRent = model.PrepaidRent.Value,
                Rent = model.Rent.Value,
                RentalPeriod = model.RentalPeriod,
                Rooms = model.Rooms.Value,
                Size = model.Size.Value,
                TakeoverDate = model.TakeoverDate,
                Title = model.Title,
                Type = model.Type
            };

            if (residence.Images == null || !residence.Images.Any()) {
                residence.Images = new List<ResidenceImage>();
                residence.Images.Add(new ResidenceImage{Image = "https://grumsenit.dk/placeholder.jpg" });
            }

            residence.UserId = HttpContext.Current.User.Identity.GetUserId();

            // FETCH LOCATION FROM GOOGLE GEOLOCATION API HERE
            residence.Position = new Facade().GetGeolocaterGateway().GetPosition(residence.Address);

            _residenceRepository.Create(residence);

            return Ok();
        }

        // DELETE: api/Residences/5
        [Authorize]
        [ResponseType(typeof(Residence))]
        public IHttpActionResult DeleteResidence(int id) {
            var deleted = _residenceRepository.Delete(id);
            if (!deleted) {
                return NotFound();
            }

            return Ok();
        }

        private ResidenceGetBindingModel GetModelFromResidence(Residence residence) {
            return new ResidenceGetBindingModel {
                Id = residence.Id,
                Aconto = residence.Aconto,
                Active = residence.Active,
                Address = residence.Address,
                Animal = residence.Animal,
                Deposit = residence.Deposit,
                Description = residence.Description,
                Energy = residence.Energy,
                Furnished = residence.Furnished,
                Images = residence.Images,
                PrepaidRent = residence.PrepaidRent,
                Rent = residence.Rent,
                RentalPeriod = residence.RentalPeriod,
                Rooms = residence.Rooms,
                Size = residence.Size,
                TakeoverDate = residence.TakeoverDate,
                Title = residence.Title,
                Type = residence.Type,
                UserId = residence.UserId,
                CreationDate = residence.CreationDate,
                Position = residence.Position
            };
        }

    }
}