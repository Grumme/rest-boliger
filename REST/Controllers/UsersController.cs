﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using DAL;
using DAL.Context;
using DAL.Entities;
using DAL.Repository;
using REST.Authorization;
using REST.Models;

namespace REST.Controllers
{
    public class UsersController : ApiController
    {
        private readonly IRepository<User, String> _userRepository = new Facade().GetUserRepository();

        // GET: api/Users
        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public List<User> GetUsers()
        {
            return _userRepository.Read();
        }

        // GET: api/Users/5
        /// <summary>
        /// Get a specific user
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(UserGetBindingModel))]
        public UserGetBindingModel GetUser(string id) {

            var user = _userRepository.Read(id);

            var model = new UserGetBindingModel {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Address = user.Address,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                Residences = user.Residences
            };

            return model;
        }

        /// <summary>
        /// Update user by id. 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        // PUT: api/Users/5
        [ResponseType(typeof(void))]
        [Authorize]
        public IHttpActionResult PutUser( UserPutBindingModel userModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new User {
                Id = userModel.Id,
                FirstName = userModel.FirstName,
                LastName = userModel.LastName,
                PhoneNumber = userModel.PhoneNumber,
                Address = userModel.Address
            };


            if (_userRepository.Update(user) != null) {
                return Ok();
            }

            return BadRequest();
        }
    }
}