﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAL.Entities;

namespace REST.Models
{
    public class UserGetBindingModel
    {
        public string Id { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PhoneNumber { get; set; }

        public Address Address { get; set; }

        public List<Residence> Residences { get; set; }
    }

    public class UserPutBindingModel {
        public string Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PhoneNumber { get; set; }

        public Address Address { get; set; }
    }
}