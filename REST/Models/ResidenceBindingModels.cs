﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DAL.Entities;

namespace REST.Models
{
    public class ResidencePostBindingModel
    {
        [Required(ErrorMessage = "Aconto feltet skal udfyldes")]
        public int? Aconto { get; set; }

        [Required]
        public Address Address { get; set; }

        [Required(ErrorMessage = "Dyr tilladt feltet skal udfyldes")]
        public bool? Animal { get; set; }

        [Required(ErrorMessage = "Indskud feltet skal udfyldes")]
        public int? Deposit { get; set; }

        [Required(ErrorMessage = "Beskrivelse feltet skal udfyldes")]
        public String Description { get; set; }

        [Required(ErrorMessage = "Energi feltet skal udfyldes")]
        public String Energy { get; set; }

        [Required(ErrorMessage = "Møbel feltet skal udfyldes")]
        public bool? Furnished { get; set; }

        public List<ResidenceImage> Images { get; set; }

        [Required(ErrorMessage = "Forudbetalt husleje feltet skal udfyldes")]
        public int? PrepaidRent { get; set; }

        [Required(ErrorMessage = "Husleje feltet skal udfyldes")]
        public int? Rent { get; set; }

        public String RentalPeriod { get; set; }

        [Required(ErrorMessage = "Rum feltet skal udfyldes")]
        public int? Rooms { get; set; }

        [Required(ErrorMessage = "Størrelse feltet skal udfyldes")]
        public int? Size { get; set; }

        public String TakeoverDate { get; set; }

        [Required(ErrorMessage = "Titel feltet skal udfyldes")]
        public String Title { get; set; }

        [Required(ErrorMessage = "Type feltet skal udfyldes")]
        public String Type { get; set; }
    }

    public class ResidenceGetBindingModel
    {
        public int Id { get; set; }
        public int Aconto { get; set; }
        public bool Active { get; set; }
        public Address Address { get; set; }
        public bool Animal { get; set; }
        public DateTime CreationDate { get; set; }
        public int Deposit { get; set; }
        public String Description { get; set; }
        public String Energy { get; set; }
        public bool Furnished { get; set; }
        public List<ResidenceImage> Images { get; set; }
        public int PrepaidRent { get; set; }
        public int Rent { get; set; }
        public String RentalPeriod { get; set; }
        public int Rooms { get; set; }
        public int Size { get; set; }
        public String TakeoverDate { get; set; }
        public String Title { get; set; }
        public String Type { get; set; }
        public String UserId { get; set; }
        public Position Position { get; set; }
    }
}