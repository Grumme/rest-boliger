﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using DAL.Entities;
using Microsoft.AspNet.Identity;
using DbContext = DAL.Context.DbContext;

namespace DAL.Repository {
    class ResidenceRepository : IRepository<Residence, int> {
        public Residence Create(Residence element) {
            using (var db = new DbContext()) {
                element.CreationDate = DateTime.Now;

                db.Residences.Add(element);
                db.SaveChanges();

                return element;
            }
        }

        public Residence Read(int id) {
            using (var db = new DbContext()) {
                return db.Residences.Include(x => x.Address).Include(x => x.Images).Include(x => x.Position).FirstOrDefault(x => x.Id == id);
            }
        }

        public List<Residence> Read() {
            using (var db = new DbContext()) {
                return db.Residences.Include(x => x.Address).Include(x => x.Images).Include(x => x.Position).ToList();
            }
        }

        public Residence Update(Residence element) {
            using (var db = new DbContext()) {
                var residenceDb = db.Residences.Include(x => x.Address).FirstOrDefault(x => x.Id == element.Id);

                if (residenceDb == null) {
                    return null;
                }

                residenceDb = element;
                db.SaveChanges();

                return element;
            }
        }

        public bool Delete(int id) {
            using (var db = new DbContext()) {
                var residenceDb = db.Residences.Include(x => x.Address).FirstOrDefault(x => x.Id == id);
                if (residenceDb == null) {
                    return false;
                }
                db.Residences.Remove(residenceDb);
                db.SaveChanges();
                return true;
            }
        }
    }
}