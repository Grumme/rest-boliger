﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Context;
using System.Web;
using Microsoft.AspNet.Identity;

using DbContext = DAL.Context.DbContext;

namespace DAL.Repository
{
    class UserRepository : IRepository<User, String>
    {
        public User Create(User element)
        {
            throw new NotImplementedException();
        }

        public User Read(string id)
        {
            using (var db = new DbContext())
            {
                return db.Users.Include(x => x.Residences.Select(y => y.Address)).Include(x => x.Residences.Select(y => y.Images)).Include(x => x.Address).FirstOrDefault(x => x.Id == id);
            }
        }

        public List<User> Read()
        {
            using (var db = new DbContext())
            {
                return db.Users.ToList();
            }
        }

        public User Update(User element)
        {
           using (var db = new DbContext())
            {
                var user = db.Users.Find(element.Id);
                var userLoggedInId = HttpContext.Current.User.Identity.GetUserId();

                if (element.Id == userLoggedInId)
                {
                    user.FirstName = element.FirstName;
                    user.LastName = element.LastName;
                    user.PhoneNumber = element.PhoneNumber;
                    user.Address = element.Address;

                    db.SaveChanges();
                    return user;
                }

                return null;
            }
        }

        public bool Delete(string id)
        {
            using (var db = new DbContext())
            {
                var elementToDelete = db.Users.FirstOrDefault(x => x.Id == id);
                db.Users.Remove(elementToDelete);
                db.SaveChanges();
                return true;
            }
        }
    }
}
