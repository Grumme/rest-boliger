﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DAL.Context
{
    public class DbContext : IdentityDbContext<User> {
        public DbSet<Residence> Residences { get; set; }

        public DbContext() : base("name=AmazonDB")
        {
            Database.SetInitializer(new DbSeeder());

            Configuration.ProxyCreationEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);


            modelBuilder.Entity<Residence>().HasMany(x => x.Images);
            // modelBuilder.Entity<Room>().HasMany(x => x.Equipment).WithMany();
            // modelBuilder.Entity<Booking>().HasMany(x => x.Invited).WithMany();
        }

        public static DbContext Create()
        {
            return new DbContext();
        }
    }
}
