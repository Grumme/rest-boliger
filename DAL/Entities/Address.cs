﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities {
    public class Address {
        public int Id { get; set; }
        [Required(ErrorMessage = "Postnummer feltet skal udfyldes")]
        public int? Zip { get; set; }
        [Required(ErrorMessage = "By feltet skal udfyldes")]
        public String City { get; set; }
        [Required(ErrorMessage = "Vej feltet skal udfyldes")]
        public String Street { get; set; }
       
    }
}