﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities {
    public class Residence {
        public int Id { get; set; }
        public int Aconto { get; set; }
        public bool Active { get; set; }
        public Address Address { get; set; }
        public bool Animal { get; set; }
        public DateTime CreationDate { get; set; }
        public int Deposit { get; set; }
        public String Description { get; set; }
        public String Energy { get; set; }
        public bool Furnished { get; set; }
        public List<ResidenceImage> Images { get; set; }
        public int PrepaidRent { get; set; }
        public int Rent { get; set; }
        public String RentalPeriod { get; set; }
        public int Rooms { get; set; }
        public int Size { get; set; }
        public String TakeoverDate { get; set; }
        public String Title { get; set; }
        public String Type { get; set; }
        public String UserId { get; set; }
        public Position Position { get; set; }
    }

    public class ResidenceImage {
        public int Id { get; set; }
        public String Image { get; set; }
    }
}