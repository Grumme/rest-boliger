﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Gateways;
using DAL.Repository;

namespace DAL {
    public class Facade {
        public IRepository<User, String> GetUserRepository() {
            return new UserRepository();
        }

        public IRepository<Residence, int> GetResidenceRepository() {
            return new ResidenceRepository();
        }

        public IGeolocaterGateway GetGeolocaterGateway() {
            return new GeolocaterGateway();
        }
    }
}