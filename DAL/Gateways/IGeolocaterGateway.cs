﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Entities;

namespace DAL.Gateways
{
    public interface IGeolocaterGateway {
        Position GetPosition(Address address);
    }
}
