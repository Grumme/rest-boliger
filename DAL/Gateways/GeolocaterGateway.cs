﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using DAL.Entities;
using Newtonsoft.Json.Linq;

namespace DAL.Gateways {
    class GeolocaterGateway : IGeolocaterGateway {
        private const string GOOGLE_API_KEY = "AIzaSyC3CbaxmQWO6Pullb8UAuVqwMtpge-1Cp4";

        public Position GetPosition(Address address) {
            using (var client = new HttpClient()) {
                String url = string.Format("https://maps.googleapis.com/maps/api/geocode/json?address={0},{1}&key={2}",
                    address.Street, address.City, GOOGLE_API_KEY);

                var response = client.GetAsync(url).Result;

                if (response.IsSuccessStatusCode) {
                    var responseJson = response.Content.ReadAsStringAsync().Result;
                    var jObject = JObject.Parse(responseJson);

                    var status = jObject["status"].ToString();
                    if (status.Equals("ZERO_RESULTS")) {
                        return new Position {Latitude = 0.0, Longitude = 0.0};
                    }

                    var results = jObject["results"][0];
                    var geometry = results["geometry"];
                    Debug.WriteLine(geometry.ToString());
                    var location = geometry["location"];
                    return new Position {
                        Latitude = Double.Parse(location["lat"].ToString()),
                        Longitude = Double.Parse(location["lng"].ToString())
                    };
                }
                return new Position { Latitude = 0.0, Longitude = 0.0 };
            }
        }
    }
}